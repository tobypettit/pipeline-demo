pipeline {
    agent any
    parameters {
        string(name: 'branch', defaultValue: 'master')
        booleanParam(name: 'delete_workspace', defaultValue: true)
        string(name: 'load_balancer_dir', defaultValue: '')
    }
    environment {
        AWS_SECRET_ACCESS_KEY   = credentials('aws_secret_access_key')
        AWS_ACCESS_KEY_ID       = credentials('aws_access_key_id')
    }
    stages {
        stage('Cleanup') {
            when {
                expression { return params.delete_workspace }
            }
            steps {
                deleteDir()
            }
        }
        stage('Setup') {
            steps {
                checkout(
                    [
                        $class: 'GitSCM',
                        branches: [[
                            name: '*/master'
                        ]],
                        doGenerateSubmoduleConfigurations: false,
                        extensions: [[
                            $class: 'SubmoduleOption',
                            disableSubmodules: false,
                            parentCredentials: false,
                            recursiveSubmodules: true,
                            reference: '',
                            trackingSubmodules: false
                        ]],
                        submoduleCfg: [],
                        userRemoteConfigs: [[
                            url: 'https://bitbucket.org/tobypettit/pipeline-demo'
                        ]]
                    ]
                )
                sh '''
                    mkdir terraform && cd terraform
                    curl https://releases.hashicorp.com/terraform/0.11.6/terraform_0.11.6_linux_amd64.zip > terraform.zip
                    unzip terraform.zip
                    export PATH=$PWD:$PATH

                    cd ../$load_balancer_dir

                    terraform init
                '''
            }
        }
        stage('Plan') {
            steps {
                sh '''
                    export PATH=$PWD/terraform:$PATH
                    terraform plan
                '''
            }
        }
        stage('Approval') {
            steps {
                input ('Apply changes?')
            }
        }
        stage('Apply') {
            steps {
                sh '''
                    export PATH=$PWD/terraform:$PATH
                    terraform apply -auto-approve
                '''
            }
        }
    }
}